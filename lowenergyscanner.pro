TARGET = blescanner
INCLUDEPATH += .

QT += bluetooth
QT -= gui

CONFIG   += console
CONFIG   -= app_bundle

# Input
SOURCES += main.cpp \
    device.cpp \
    deviceinfo.cpp \
    serviceinfo.cpp \
    characteristicinfo.cpp

HEADERS += \
    device.h \
    deviceinfo.h \
    serviceinfo.h \
    characteristicinfo.h

RESOURCES +=

INSTALLS += target
